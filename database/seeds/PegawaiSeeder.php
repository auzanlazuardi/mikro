<?php

use Illuminate\Database\Seeder;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pegawai')->delete();

        $data = [
            [
                'branch_id' => '11935',
                'pn' => '12345',
                'nama' =>  'Luqman Hakim',
                'role' => 'mka',
            ],
            [
                'branch_id' =>  '11935',
                'pn' => '09876',
                'nama' =>  'Hesti',
                'role'  => 'mks',
            ],
            [
                'branch_id' =>  '11935',
                'pn' => '67890',
                'nama' =>  'Vika',
                'role'  => 'mbm',
            ]
        ];

        \App\Pegawai::insert($data);
    }
}

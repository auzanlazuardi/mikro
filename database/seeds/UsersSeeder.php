<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $data = [
          [
              'name' => 'Luqman Hakim',
              'email' => 'luqman@gmail.com',
              'password' => bcrypt('secret'),
              'username' => '12345',
              'role'    => 'mka'
          ],
            [
                'name' => 'Vika',
                'email' => 'vika@gmail.com',
                'password' => bcrypt('secret'),
                'username' => '67890',
                'role'    => 'mbm'
            ],
            [
                'name' => 'Hesti',
                'email' => 'hesti@gmail.com',
                'password' => bcrypt('secret'),
                'username' => '09876',
                'role'    => 'mks'
            ],
            [
                'name' => 'Auzan Lazuardi',
                'email' => 'auzan@gmail.com',
                'password' => bcrypt('secret'),
            ]
        ];
        foreach($data as $data) {
            \App\User::create($data);
        }

    }
}

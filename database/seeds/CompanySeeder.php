<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company')->delete();

        $data = [
            ['branch_id' => '11935','nama_company' => 'Garuda Indonesia', 'kode' => '1'],
            ['branch_id' => '11901','nama_company' => 'PT Metrodata', 'kode' => '2'],
            ['branch_id' => '11935','nama_company' => 'PT Bank Mandiri', 'kode' => '3'],
            ['branch_id' => '11903','nama_company' => 'PT Bukit Asam', 'kode' => '4'],
            ['branch_id' => '11902','nama_company' => 'PT Pusri Palembang', 'kode' => '5'],
        ];

        \App\Company::insert($data);

    }
}

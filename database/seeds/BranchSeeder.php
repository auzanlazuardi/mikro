<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->delete();

        $data = [
            ['nama_cabang' => 'KCP Mangga Dua','provinsi' => 'Jakarta Pusat', 'kodepos' => '10730', 'kode_branch' => '11935'],
            ['nama_cabang' => 'KCP Jakarta Angkasa','provinsi' => 'Jakarta Pusat', 'kodepos' => '10610', 'kode_branch' => '11901'],
            ['nama_cabang' => 'KCP Jakarta Gambir','provinsi' => 'Jakarta Pusat', 'kodepos' => '10120', 'kode_branch' => '11902'],
            ['nama_cabang' => 'KCP Arteri Pondok Indah','provinsi' => 'Jakarta Selatan', 'kodepos' => '12310', 'kode_branch' => '11903'],

        ];

        \App\Branch::insert($data);
    }
}

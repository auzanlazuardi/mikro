<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('nik')->nullable();
            $table->string('npwp')->nullable();
            $table->string('hp')->nullable();
            $table->string('alamat')->nullable();
            $table->string('status_tinggal')->nullable();
            $table->string('jenis_perusahaan')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->string('alamat_perusahaan')->nullable();
            $table->string('gaji')->nullable();
            $table->string('penghasilan_lain')->nullable();
            $table->string('emergency_status')->nullable();
            $table->string('emergency_nama')->nullable();
            $table->string('emergency_hp')->nullable();
            $table->string('emergency_alamat')->nullable();
            $table->string('emergency_kota')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('jabatan')->nullable();
            $table->string('pinjaman_lain')->nullable();
            $table->string('status_pegawai')->nullable();
            $table->string('gender')->nullable();
            $table->string('status_nikah')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('jabatan');
            $table->dropColumn('pinjaman_lain');
            $table->dropColumn('status_pegawai');
            $table->dropColumn('gender');
            $table->dropColumn('status_nikah');
        });
    }
}

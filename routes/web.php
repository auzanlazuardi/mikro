<?php

Route::group([
    'middleware' => 'auth'
], function() {

    //API!!
    Route::get('/eforms/mbm', 'EformController@getData')->name('eform.get.mbm');
    //API!!

    Route::get('/', 'DashboardController@index')->name('beranda');
    Route::get('/pengajuan', 'PengajuanController@index')->name('pengajuan');
    Route::get('/pengajuan/ksm', 'PengajuanController@indexKsm')->name('pengajuan.ksm');
    Route::post('/pengajuan/ksm', 'PengajuanController@indexKsm')->name('pengajuan.ksm.post');
    Route::post('/pengajuan/ksms', 'PengajuanController@postKsm')->name('pengajuan.ksm.post.done');
    Route::get('/monitoring', 'MonitoringController@index')->name('monitoring');
    Route::get('/monitoring/modal', 'MonitoringController@getModal')->name('monitoring.modal');
    Route::get('/kalkulator', 'CalculatorController@index')->name('kalkulator');
    Route::post('/kalkulator', 'CalculatorController@index')->name('kalkulator.post');

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/profile', 'ProfileController@addOrEdit')->name('profile.post');

    //MBM
    Route::get('/disposisi', 'MBMController@index')->name('mbm.disposisi');
    Route::get('/disposisi/{id}', 'MBMController@show')->name('mbm.disposisi.show');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

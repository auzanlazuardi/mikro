@extends('master.layout')

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
                <h3 class="kt-portlet__head-title">
                    Monitoring
                    <small>kamu bisa memonitoring status pengajuan kredit disini</small>
                </h3>
            </div>

        </div>
        @if(!empty($eform))
        @foreach($eform as $eform)
        <div class="kt-portlet__body">
            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--bordered">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ $eform['tipeProduk'] }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-4">
                            <h6>No Aplikasi</h6>
                            <p>{{ $eform['appno'] }}</p>

                            <h6>Tipe Produk</h6>
                            <p>{{ $eform['product_type'] }}</p>

                            <h6>Status Pengajuan</h6>
                            <p><span class="kt-font-boldest">{{ $eform['status'] }}</span></p>
                        </div>
                        <div class="col-md-4">
                            <h6>Limit</h6>
                            <p>Rp {{ format_rupiah($eform['limit']) }}</p>

                            <h6>Angsuran per Bulan</h6>
                            <p>Rp {{ format_rupiah($eform['angsuran']) }}</p>

                            <h6>Tenor</h6>
                            <p>{{ $eform['tenor'] }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Cabang Pemroses</h6>
                            <p>{{ $eform->branch->nama_cabang }}</p>

                            <h6>Sales</h6>
                            <p>{{ $eform->pegawai->nama ?? '-' }}</p>
                        </div>
                    </div>

                    @if($eform['status'] == 'Pending')
                        <div class="alert alert-warning" role="alert">
                            Pengajuan kamu sedang dalam proses verifikasi
                        </div>
                    @else
                        <div class="alert alert-success" role="alert">
                            Selamat! dana anda telah cair dan akan dimasukkan kedalam rekening 1x24 jam
                        </div>
                    @endif
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        @endforeach
        @endif
    </div>

    
@endsection

@section('script')
    <script src="{{ asset('assets/app/custom/general/crud/metronic-datatable/base/html-table.js') }}" type="text/javascript"></script>

    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @php
            Session::forget('success');
        @endphp
        @endif

        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @php
            Session::forget('error');
        @endphp
        @endif
    </script>
@endsection

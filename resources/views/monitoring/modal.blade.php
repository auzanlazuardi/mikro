<!--begin::Modal-->
<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Status Pengajuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6>No Aplikasi</h6>
                        <p>{{ $modal['appno'] }}</p>

                        <h6>Tipe Produk</h6>
                        <p>{{ $modal['product_type'] }}</p>

                        <h6>Status Pengajuan</h6>
                        <p><span class="kt-font-boldest">{{ $modal['status'] }}</span></p>
                    </div>
                    <div class="col-md-6">
                        <h6>Limit</h6>
                        <p>Rp {{ format_rupiah($modal['limit']) }}</p>

                        <h6>Angsuran per Bulan</h6>
                        <p>Rp {{ format_rupiah($modal['angsuran']) }}</p>

                        <h6>Tenor</h6>
                        <p>{{ $modal['tenor'] }}</p>
                    </div>
                </div>
                <div class="alert alert-warning" role="alert">
                    <strong>Well done!</strong> You successfully read this important alert message.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->

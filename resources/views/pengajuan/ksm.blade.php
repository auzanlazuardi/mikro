@extends('master.layout')

@section('content')
    <div class="row">
        @if(isset($message))
        @else
        <div class="col-lg-6">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                    </span>
                        <h3 class="kt-portlet__head-title">
                            
                        </h3>
                    </div>

                </div>

                <!--begin::Form-->
                <form class="kt-form" method="post" action="{{ route('pengajuan.ksm.post') }}">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">No Rekening:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="nik" maxlength="13" id="kt_inputmask_10" required>
                                    </div>
                                </div>
                            
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tujuan Penggunaan Kredit</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <select class="form-control kt-selectpicker" name="emergency_status" >
                                            <option value="1">Pendidikan</option>
                                            <option value="1">Rumah Sakit</option>
                                            <option value="1">Rumah Tangga</option>
                                            <option value="1">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Jumlah Limit:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="price" id="suite" maxlength="12" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Tenor:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="term" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success">Selanjutnya</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->

            </div>
        </div>
        @endif
        @if(isset($message))
        <div class="col-lg-6">
            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Hasil Perhitungan Biaya Efektif
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="alert alert-{{ $message['alert'] }}" role="alert">
                        <div class="alert-text">{{ $message['message'] }}</div>
                    </div>
                    <h6>Jumlah Limit:</h6>
                    <p> Rp {{ $rincian['plafond'] }}</p>

                    <h6>Angsuran per Bulan: </h6>
                    <p>Rp {{ $rincian['angsuran_perbulan'] }}</p>

                    <h6>Lama Pinjaman: </h6>
                    <p>{{ $rincian['lama_pinjaman'] }} Bulan</p>

                    <h6>Biaya Administrasi: </h6>
                    <p>Rp {{ $rincian['admin'] }}</p>

                    <h6>Biaya Provisi: </h6>
                    <p>Rp {{ $rincian['provisi'] }}</p>
                    @if($message['alert'] == 'danger')
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <a href="{{ route('pengajuan.ksm') }}"><button type="button" class="btn btn-warning">Hitung Ulang</button></a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
            @if($message['alert'] == 'danger')
            @else
            <div class="col-lg-6">
                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                            <h3 class="kt-portlet__head-title">
                                Upload Dokumen Kamu
                            </h3>
                        </div>

                    </div>

                    <!--begin::Form-->
                    <form class="kt-form" method="post" action="{{ route('pengajuan.ksm.post.done') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="limit" value="{{ $rincian['plafond'] }}">
                        <input type="hidden" name="angsuran" value="{{ $rincian['angsuran_perbulan'] }}">
                        <input type="hidden" name="tenor" value="{{ $rincian['lama_pinjaman'] }}">

                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">KTP:</label>
                                        <div class="col-lg-6">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile" name="ktp">
                                                <label class="custom-file-label" for="customFile">Pilih File</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">NPWP:</label>
                                        <div class="col-lg-6">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile" name="npwp">
                                                <label class="custom-file-label" for="customFile">Pilih File</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Kartu Keluarga:</label>
                                        <div class="col-lg-6">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile" name="kk">
                                                <label class="custom-file-label" for="customFile">Pilih File</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">Ajukan</button>
                                        <a href="{{ route('pengajuan.ksm') }}"><button type="button" class="btn btn-danger">Hitung Ulang</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->

                </div>
            </div>
            @endif
        @endif
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        /* Without prefix */
        var suite = document.getElementById('suite');
        var suite2 = document.getElementById('suite2');

        suite.addEventListener('keyup', function(e)
        {
            suite.value = format_number(this.value);

        });
        suite2.addEventListener('keyup', function(e)
        {
            suite2.value = format_number(this.value);

        });

        /* Function */
        function format_number(number, prefix, thousand_separator, decimal_separator)
        {
            var thousand_separator = thousand_separator || '.',
                decimal_separator = decimal_separator || ',',
                regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
                number_string = number.replace(regex, '').toString(),
                split	  = number_string.split(decimal_separator),
                rest 	  = split[0].length % 3,
                result 	  = split[0].substr(0, rest),
                thousands = split[0].substr(rest).match(/\d{3}/g);

            if (thousands) {
                separator = rest ? thousand_separator : '';
                result += separator + thousands.join(thousand_separator);
            }
            result = split[1] != undefined ? result + decimal_separator + split[1] : result;
            return prefix == undefined ? result : (result ? prefix + result : '');
        };
    </script>

    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/bootstrap-maxlength.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/input-mask.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>

    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @php
            Session::forget('success');
        @endphp
        @endif

        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @php
            Session::forget('error');
        @endphp
        @endif
    </script>

@endsection

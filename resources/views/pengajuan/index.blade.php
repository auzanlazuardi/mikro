@extends('master.layout')

@section('content')
    @if($user_id == Auth::user()->id)

        <a href="{{ route('pengajuan.ksm') }}">
            <div class="kt-section">
                <div class="col-md-3">
                <div class="kt-section__content kt-section__content--solid">
                    <center><img src="{{ asset('assets/media/pengajuan-ksm.jpg') }}" width="230px" height="230px"/>

                        <div class="kt-divider">
                            <span></span>
                        </div>
                        <h4 style="margin-top: 20px">Kredit Serbaguna Mandiri</h4></center>
                </div>
            </div>
            </div>
        </a>

    @else
        <div class="alert alert-danger" role="alert">
            <div class="alert-text">
                <h4 class="alert-heading">Yuk Lengkapi Data Dulu</h4>
                <p>Halo {{ Auth::user()->name }}, sebelum melakukan pengajuan silahkan edit profile kamu terlebih dahulu untuk memudahkan proses pengajuan.</p>
                <hr>
                <a href="{{ route('profile') }}"><button type="button" class="btn btn-brand">Edit Profile</button></a>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @php
            Session::forget('success');
        @endphp
        @endif

        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @php
            Session::forget('error');
        @endphp
        @endif
    </script>
@endsection

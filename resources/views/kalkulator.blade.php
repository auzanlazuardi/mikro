@extends('master.layout')

@section('content')
    <div class="row">
        <div class="col-lg-6">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Perhitungan
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->

                <form class="kt-form" method="POST" action="{{ route('kalkulator.post') }}">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Tipe Suku Bunga</label>
                                <div class="col-lg-6 col-md-9 col-sm-12">
                                    <select class="form-control kt-selectpicker" name="type">
                                        <option value="1">Flat</option>
                                        <option value="2">Efektif</option>
                                    </select>
                                </div>
                            </div>
                            <h3 class="kt-section__title">Data Perhitungan:</h3>
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Plafond (Rp)</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="price" id="suite" required>
                                        <span class="form-text text-muted">Jumlah uang pinjaman</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tenor</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <select class="form-control kt-selectpicker" name="term">
                                            <option value="12">1 Tahun</option>
                                            <option value="24">2 Tahun</option>
                                            <option value="36">3 Tahun</option>
                                            <option value="48">4 Tahun</option>
                                            <option value="60">5 Tahun</option>
                                            <option value="72">6 Tahun</option>
                                            <option value="84">7 Tahun</option>
                                            <option value="96">8 Tahun</option>
                                            <option value="108">9 Tahun</option>
                                            <option value="120">10 Tahun</option>
                                            <option value="132">11 Tahun</option>
                                            <option value="144">12 Tahun</option>
                                            <option value="156">13 Tahun</option>
                                            <option value="168">14 Tahun</option>
                                            <option value="180">15 Tahun</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Suku Bunga (%)</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="rate" id="wkwk" required>
                                        <span class="form-text text-muted">Suku bunga per tahun</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>

        @if(isset($rincian))
        <div class="col-lg-6">
        <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Hasil Perhitungan Biaya
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <h6>Jumlah Plafond:</h6>
                <p> Rp {{ $rincian['plafond'] ?? ''}}</p>

                <h6>Angsuran per Bulan: </h6>
                <p>Rp {{ $rincian['angsuran_perbulan'] ?? '' }}</p>

                <h6>Lama Pinjaman: </h6>
                <p>{{ $rincian['lama_pinjaman'] ?? '' }}</p>
            </div>
        </div>

        <!--end::Portlet-->

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Detail Tabel Angsuran
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Accordion-->
                    @foreach($tabel as $dataT)
                    <div class="accordion" id="accordionExample1">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse{{ $dataT['bulan'] }}" aria-expanded="false" aria-controls="collapse{{ $dataT['bulan'] }}">
                                    Bulan - {{ $dataT['bulan'] }}
                                </div>
                            </div>
                            @if($dataT['bulan'] == 0)
                            <div id="collapse{{ $dataT['bulan'] }}" class="collapse show" aria-labelledby="heading{{ $dataT['bulan'] }}" data-parent="#accordionExample1">
                            @else
                            <div id="collapse{{ $dataT['bulan'] }}" class="collapse" aria-labelledby="heading{{ $dataT['bulan'] }}" data-parent="#accordionExample1">
                            @endif
                                <div class="card-body">
                                    <h6>Jumlah Plafond:</h6>
                                    <p> Rp {{ $dataT['sisa_pinjaman'] ?? ''}}</p>

                                    <h6>Angsuran Pokok: </h6>
                                    <p>Rp {{$dataT['angsuran_pokok'] ?? '' }}</p>

                                    <h6>Angsuran Bunga: </h6>
                                    <p>Rp {{ $dataT['angsuran_bunga'] ?? '' }}</p>

                                    <h6>Angsuran per bulan: </h6>
                                    <p>Rp {{ $dataT['angsuran'] ?? '' }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!--end::Accordion-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
        @endif
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        /* Without prefix */
        var suite = document.getElementById('suite');

        suite.addEventListener('keyup', function(e)
        {
            suite.value = format_number(this.value);

        });

        /* Function */
        function format_number(number, prefix, thousand_separator, decimal_separator)
        {
            var thousand_separator = thousand_separator || '.',
                decimal_separator = decimal_separator || ',',
                regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
                number_string = number.replace(regex, '').toString(),
                split	  = number_string.split(decimal_separator),
                rest 	  = split[0].length % 3,
                result 	  = split[0].substr(0, rest),
                thousands = split[0].substr(rest).match(/\d{3}/g);

            if (thousands) {
                separator = rest ? thousand_separator : '';
                result += separator + thousands.join(thousand_separator);
            }
            result = split[1] != undefined ? result + decimal_separator + split[1] : result;
            return prefix == undefined ? result : (result ? prefix + result : '');
        };
    </script>

    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
@endsection

@extends('master.layout')
@section('content')
    <div class="alert alert-success" role="alert">
        <div class="alert-text">
            <h4 class="alert-heading">Selamat datang kembali, {{ Auth::user()->name }}!</h4>
            <p>Mandiri E-Kredit memberikan fasilitas bagi anda untuk mengajukan kredit secara mudah,cepat dan aman. Aplikasi juga dapat anda download melalui Google Play Store maupun Apple Store</p>
            <hr>
            <p class="mb-0">Terima kasih telah menggunakan layanan dan produk kami.</p>
        </div>
    </div>
    <center><img src="{{ asset('assets/media/logomandiri.png') }}" height="500px" /></center>
@endsection

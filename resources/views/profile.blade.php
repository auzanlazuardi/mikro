@extends('master.layout')

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
                <h3 class="kt-portlet__head-title">
                    Biodata
                </h3>
            </div>

        </div>

        <!--begin::Form-->
        <form class="kt-form" method="post" action="{{ route('profile.post') }}">
            @csrf
            <div class="kt-portlet__body">
                <div class="kt-section kt-section--first">
                    <h3 class="kt-section__title">1. Data Pribadi</h3>
                    <div class="kt-section__body">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">No KTP:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="nik" value="{{ $nik ?? '' }}" minlength="16" id="kt_inputmask_10" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">NPWP:</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="npwp" value="{{ $npwp ?? '' }}" id="kt_inputmask_8" minlength="15" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nama Lengkap (Sesuai Identitas):</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Email</label>
                            <div class="col-lg-6">
                                <input type="email" class="form-control" placeholder="{{ Auth::user()->email }}" disabled>
                                <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Telepon Seluler</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="hp" value="{{ $hp ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Tempat Lahir</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="alamat" value="{{ $alamat ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="kt_datepicker_1" readonly placeholder="Pilih tanggal"  required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Alamat Domisili</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="alamat" value="{{ $alamat ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Status Tempat Tinggal</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select class="form-control kt-selectpicker" name="status_tinggal">
                                    @if(!empty($status_tinggal) AND $status_tinggal == 1)
                                        <option value="1" selected>Milik Sendiri</option>
                                    @else
                                        <option value="1">Milik Sendiri</option>
                                    @endif

                                    @if(!empty($status_tinggal) AND $status_tinggal == 2)
                                        <option value="2" selected>Keluarga</option>
                                    @else
                                        <option value="2">Keluarga</option>
                                    @endif

                                    @if(!empty($status_tinggal) AND $status_tinggal == 3)
                                        <option value="3" selected>Sewa/Kost/Kontrak</option>
                                    @else
                                        <option value="3">Sewa/Kost/Kontrak</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Jenis Kelamin</label>
                            <div class="col-lg-6">
                                <div class="kt-radio-inline">
                                    @if(!empty($status_tinggal) AND $gender == 1)
                                        <label class="kt-radio">
                                            <input type="radio" name="gender" value="1" checked> Laki-Laki
                                            <span></span>
                                        </label>
                                    @else
                                        <label class="kt-radio">
                                            <input type="radio" name="gender" value="1"> Laki-Laki
                                            <span></span>
                                        </label>
                                    @endif
                                    @if(!empty($gender) AND $gender == 2)
                                        <label class="kt-radio">
                                            <input type="radio" name="gender" value="1" checked> Perempuan
                                            <span></span>
                                        </label>
                                    @else
                                        <label class="kt-radio">
                                            <input type="radio" name="gender" value="1"> Perempuan
                                            <span></span>
                                        </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Status Pernikahan</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select class="form-control kt-selectpicker" name="status_nikah">
                                    @if(!empty($status_nikah) AND $status_nikah == 1)
                                        <option value="1" selected>Belum Menikah</option>
                                    @else
                                        <option value="1">Belum Menikah</option>
                                    @endif

                                    @if(!empty($status_nikah) AND $status_nikah == 2)
                                        <option value="2" selected>Menikah</option>
                                    @else
                                        <option value="2">Menikah</option>
                                    @endif

                                    @if(!empty($status_nikah) AND $status_nikah == 3)
                                        <option value="3" selected>Duda/Janda</option>
                                    @else
                                        <option value="3">Duda/Janda</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <h3 class="kt-section__title">2. Data Pekerjaan</h3>
                    <div class="kt-section__body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Jenis Perusahaan/Instansi</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select class="form-control kt-selectpicker" name="jenis_perusahaan">
                                    @if(!empty($jenis_perusahaan) AND $jenis_perusahaan == 1)
                                        <option value="1" selected>PNS/TNI/POLRI</option>
                                    @else
                                        <option value="1">PNS/TNI/POLRI</option>
                                    @endif

                                    @if(!empty($jenis_perusahaan) AND $jenis_perusahaan == 2)
                                        <option value="2" selected>BUMN/BUMD</option>
                                    @else
                                        <option value="2">BUMN/BUMD</option>
                                    @endif

                                    @if(!empty($jenis_perusahaan) AND $jenis_perusahaan == 3)
                                        <option value="3" selected>SWASTA</option>
                                    @else
                                        <option value="3">SWASTA</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Nama Perusahaan/Instansi</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select class="form-control m-select2" id="kt_select2_1" name="nama_perusahaan">
                                    @foreach($company as $data)
                                        @if(!empty($nama_perusahaan) AND $data->id == $nama_perusahaan)
                                            <option value="{{ $data->kode }}" selected>{{ $data->nama_company }}</option>
                                        @else
                                            <option value="{{ $data->kode }}">{{ $data->nama_company }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Alamat Perusahaan/Instansi</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="alamat_perusahaan" value="{{ $alamat_perusahaan ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Jabatan</label>
                            <div class="col-lg-6">
                                <select class="form-control kt-selectpicker" name="jabatan" >
                                    @if(!empty($jabatan) AND $jabatan == 1)
                                        <option value="1" selected>Pemilik/Komisaris</option>
                                    @else
                                        <option value="1">Pemilik/Komisaris</option>
                                    @endif

                                    @if(!empty($jabatan) AND $jabatan == 2)
                                        <option value="2" selected>Direktur</option>
                                    @else
                                        <option value="2">Direktur</option>
                                    @endif

                                    @if(!empty($jabatan) AND $jabatan == 3)
                                        <option value="3" selected>Pengurus</option>
                                    @else
                                        <option value="3">Pengurus</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Status Kepegawaian</label>
                            <div class="col-lg-6">
                            <div class="kt-radio-inline">
                                @if(!empty($status_pegawai) AND $status_pegawai == 1)
                                    <label class="kt-radio">
                                        <input type="radio" name="status_pegawai" value="1" checked> Tetap
                                        <span></span>
                                    </label>
                                @else
                                    <label class="kt-radio">
                                        <input type="radio" name="status_pegawai" value="1"> Tetap
                                        <span></span>
                                    </label>
                                @endif
                                @if(!empty($status_pegawai) AND $status_pegawai == 2)
                                    <label class="kt-radio">
                                        <input type="radio" name="status_pegawai" value="1" checked> Kontrak
                                        <span></span>
                                    </label>
                                @else
                                    <label class="kt-radio">
                                        <input type="radio" name="status_pegawai" value="1"> Kontrak
                                        <span></span>
                                    </label>
                                @endif
                            </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="kt-section__title">3. Data Finansial</h3>
                    <div class="kt-section__body">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Gaji (Rp)</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="gaji" id="suite" value="{{ $gaji ?? '' }}" maxlength="20" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Pendapatan Lain (Rp)</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="penghasilan_lain" value="{{ $penghasilan_lain ?? '0' }}"id="suite2" maxlength="20" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Pinjaman Lain Jika Ada (Rp)</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="pinjaman_lain" value="{{ $pinjaman_lain ?? '0' }}"id="suite2" maxlength="20" required>
                            </div>
                        </div>
                    </div>
                    <h3 class="kt-section__title">4. Data Kontak Darurat - Kerabat tidak serumah</h3>
                    <div class="kt-section__body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Status Hubungan</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select class="form-control kt-selectpicker" name="emergency_status" >
                                    @if(!empty($emergency_status) AND $emergency_status == 1)
                                        <option value="1" selected>ORANG TUA</option>
                                    @else
                                        <option value="1">ORANG TUA</option>
                                    @endif

                                    @if(!empty($emergency_status) AND $emergency_status == 2)
                                        <option value="2" selected>SUAMI/ISTRI</option>
                                    @else
                                        <option value="2">SUAMI/ISTRI</option>
                                    @endif

                                    @if(!empty($emergency_status) AND $emergency_status == 3)
                                        <option value="3" selected>SAUDARA KANDUNG</option>
                                    @else
                                        <option value="3">SAUDARA KANDUNG</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Nama Lengkap</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="emergency_nama" value="{{ $emergency_nama ?? '' }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Telepon Seluler</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="emergency_hp" value="{{ $emergency_hp ?? '' }}" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">Ubah Data</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->

    </div>
@endsection

@section('script')
    <script type="text/javascript">

        /* Without prefix */
        var suite = document.getElementById('suite');
        var suite2 = document.getElementById('suite2');

        suite.addEventListener('keyup', function(e)
        {
            suite.value = format_number(this.value);

        });
        suite2.addEventListener('keyup', function(e)
        {
            suite2.value = format_number(this.value);

        });

        /* Function */
        function format_number(number, prefix, thousand_separator, decimal_separator)
        {
            var thousand_separator = thousand_separator || '.',
                decimal_separator = decimal_separator || ',',
                regex		= new RegExp('[^' + decimal_separator + '\\d]', 'g'),
                number_string = number.replace(regex, '').toString(),
                split	  = number_string.split(decimal_separator),
                rest 	  = split[0].length % 3,
                result 	  = split[0].substr(0, rest),
                thousands = split[0].substr(rest).match(/\d{3}/g);

            if (thousands) {
                separator = rest ? thousand_separator : '';
                result += separator + thousands.join(thousand_separator);
            }
            result = split[1] != undefined ? result + decimal_separator + split[1] : result;
            return prefix == undefined ? result : (result ? prefix + result : '');
        };
    </script>

    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/bootstrap-maxlength.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/input-mask.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>

    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @php
            Session::forget('success');
        @endphp
        @endif

        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @php
            Session::forget('error');
        @endphp
        @endif
    </script>

@endsection

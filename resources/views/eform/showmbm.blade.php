@extends('master.layout')

@section('content')
<div class="kt-portlet__body">
            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--bordered">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Data Nasabah / {{ $eform['appno']}}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                	<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#kt_tabs_4_1" role="tab">Data Pribadi</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_4_2" role="tab">Data Pekerjaan</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_4_3" role="tab">Data Penghasilan</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_4_4" role="tab">Data Kontak Darurat</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#kt_tabs_4_5" role="tab">Data Dokumen</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="kt_tabs_4_1" role="tabpanel">
					<div class="row">
                        <div class="col-md-4">
                            <h6>NIK</h6>
                            <p>{{ $customer->nik }}</p>

                            <h6>NPWP</h6>
                            <p>{{ $customer->npwp }}</p>

                            <h6>Nama Lengkap</h6>
                            <p>{{ $customer->user->name }}</p>

                            <h6>Handphone</h6>
                            <p>{{ $customer->hp }}</p>

                            <h6>Alamat</h6>
                            <p>{{ $customer->alamat }}</p>

                            <h6>Status Nikah</h6>
                            <p>{{ $customer->statusNikahText }}</p>

                            <h6>Jenis Kelamin</h6>
                            <p>{{ $customer->genderText }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>limit</h6>
                            <p>Rp {{ format_rupiah($eform['limit']) }}</p>

                            <h6>Angsuran per Bulan</h6>
                            <p>Rp {{ format_rupiah($eform['angsuran']) }}</p>

                            <h6>Tenor</h6>
                            <p>{{ $eform['tenor'] }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Cabang Pemroses</h6>
                            <p>{{ $eform->branch->nama_cabang }}</p>

                            <h6>Sales</h6>
                            <p>{{ $eform->pegawai->nama ?? '-' }}</p>
                        </div>
                    </div>
						</div>
						<div class="tab-pane" id="kt_tabs_4_2" role="tabpanel">
							<div class="row">
                        <div class="col-md-4">
                            <h6>Nama Perusahaan</h6>
                            <p>{{ $customer->company->nama_company }}</p>

                            <h6>Jenis Perusahaan</h6>
                            <p>{{ $customer->jenisPerusahaanText }}</p>

                            <h6>Alamat Perusahaan</h6>
                            <p>{{ $customer->alamat_perusahaan }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Jabatan</h6>
                            <p>{{ $customer->jabatanText }}</p>

                            <h6>Status Pegawai</h6>
                            <p>{{ $customer->statusPegawaiText }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Cabang Pemroses</h6>
                            <p>{{ $eform->branch->nama_cabang }}</p>

                            <h6>Sales</h6>
                            <p>{{ $eform->pegawai->nama ?? '-' }}</p>
                        </div>
                    </div>
						</div>
						<div class="tab-pane" id="kt_tabs_4_3" role="tabpanel">
							<div class="row">
                        <div class="col-md-4">
                            <h6>Penghasilan Tetap</h6>
                            <p>{{ $customer->gajiText }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Penghasilan Lainnya</h6>
                            <p>{{ $customer->penghasilanLainText }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Cabang Pemroses</h6>
                            <p>{{ $eform->branch->nama_cabang }}</p>

                            <h6>Sales</h6>
                            <p>{{ $eform->pegawai->nama ?? '-' }}</p>
                        </div>
                    </div>
						</div>
						<div class="tab-pane" id="kt_tabs_4_4" role="tabpanel">
							<div class="row">
                        <div class="col-md-4">
                            <h6>Status</h6>
                            <p>{{ $customer->statusEmergencyText }}</p>

                            <h6>Nama</h6>
                            <p>{{ $customer->emergency_nama }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Handphone</h6>
                            <p>{{ $customer->emergency_hp }}</p>
                        </div>
                        <div class="col-md-4">
                            <h6>Cabang Pemroses</h6>
                            <p>{{ $eform->branch->nama_cabang }}</p>

                            <h6>Sales</h6>
                            <p>{{ $eform->pegawai->nama ?? '-' }}</p>
                        </div>
                    </div>
						</div>
						<div class="tab-pane" id="kt_tabs_4_5" role="tabpanel">
							<!-- <div class="row">
                        <div class="col-md-6">
                            <h6>KTP</h6>
                           

                            <h6>NPWP</h6>
                          

                            <h6>Kartu Keluarga</h6>
                           

                        </div>
                        <div class="col-md-6">
                            <img src="{{ $eform->ktpImage }}"/>
                          	<img src="{{ $eform->ktpImage }}"/>
                          	<img src="{{ $eform->ktpImage }}"/>	
                        </div>
                    </div> -->
                    	<table border=1>
                    		<thead>
                    			<tr>
                    				<th width="10%"><center>1.</center></th>
	                    			<th width="60%"><center>KTP</center></th>
	                    			<th><img src="{{ $eform->ktpImage }}"/></th>
	                    		</tr>
							</thead>
							<tbody>
								<tr>
									<th><center>2.</center></th>
									<th><center>NPWP</center></th>
									<th><img src="{{ $eform->npwpImage }}"/></th>
								</tr>
								<tr>
									<th><center>3.</center></th>
									<th><center>Kartu Keluarga</center></th>
									<th><img src="{{ $eform->kkImage }}"/></th>
								</tr>
							</tbody>
                    	</table>
						</div>
					</div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
@endsection
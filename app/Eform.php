<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\User;

class Eform extends Model
{
    protected $table = 'eforms';

    protected $fillable = [
        'customer_id', 'product_type', 'appno', 'limit', 'angsuran', 'tenor',
        'pn', 'branch_id', 'status', 'ktp', 'npwp', 'kk'
    ];

    protected $appends = ['button', 'limitRp', 'namaCust', 'ktpImage', 'npwpImage', 'kkImage'];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id','kode_branch');
    }

    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'pn', 'pn');
    }

    public function getStatusNoAttribute()
    {
        if($this->status == 'Pending') {
            return '1';
        }
        elseif($this->status == 'Proses') {
            return '2';
        }
        elseif($this->status == 'Approval') {
            return '3';
        }
    }

    public function getTipeProdukAttribute()
    {
        if($this->product_type == 'ksm')
        {
            return 'Kredit Serbaguna Mandiri';
        }
        elseif($this->product_type == 'kartu_kredit')
        {
            return 'Kartu Kredit';
        }
    }

    public function getButtonAttribute()
    {
        $data['id'] = $this->id;
        return view('eform._button', $data)->render();
    }

    public function getLimitRpAttribute()
    {
        return 'Rp '.format_rupiah($this->limit);
    }

    public function getNamaCustAttribute()
    {
        $cust = (new Customer)->where('user_id', $this->customer_id)->first();
        $user = (new User)->where('id', $this->customer_id)->first();

        return $user['name'];
    }

    public function getKtpImageAttribute()
    {
        $customer = (new Customer)->where('user_id', $this->customer_id)->first();
        $user = (new User)->where('id', $this->customer_id)->first();
        return url('storage/'.$user['name'].'/'.$this->ktp);
    }

    public function getNpwpImageAttribute()
    {
        $customer = (new Customer)->where('user_id', $this->customer_id)->first();
        $user = (new User)->where('id', $this->customer_id)->first();
        return url('storage/'.$user['name'].'/'.$this->npwp);
    }

    public function getKkImageAttribute()
    {
        $customer = (new Customer)->where('user_id', $this->customer_id)->first();
        $user = (new User)->where('id', $this->customer_id)->first();
        return url('storage/'.$user['name'].'/'.$this->kk);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'kode_branch');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';

    protected $fillable = ['branch_id', 'pn', 'nama'];
    public $timestamps = false;

}

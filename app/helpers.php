<?php

if (! function_exists('format_rupiah')) {

    function format_rupiah($value)
    {
        if($value == null) {
            return '';
        }

        return number_format($value, 0, ',', '.');
    }
}


if (! function_exists('number_back')) {

    function number_back($value)
    {
        return str_replace(['.','-', '_'], '', $value);
    }
}

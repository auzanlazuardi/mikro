<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EForm;
use App\Customer;

class MBMController extends Controller
{
    public function index()
    {
        return view('mbm.disposisi');
    }

    public function disposisi()
    {
    	$eform = (new Eform)->where('branch_id', '11935')->first();

    	return view('eform._modal', $eform);
    }

    public function show($id)
    {
    	$data['eform'] 		= Eform::find($id);
    	$data['customer']	= (new Customer)->where('user_id', $data['eform']->customer_id)->first();

    	return view('eform.showmbm', $data);
    }
}

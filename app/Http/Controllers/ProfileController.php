<?php

namespace App\Http\Controllers;

use App\Company;
use App\Customer;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $customer   = (new Customer)->where('user_id', Auth::user()->id)->first();
        $customer['company']     = (new Company)->get();

        if($customer) {
            return view('profile', $customer);
        }
        else {
            $data['status_tinggal'] = null;
            $data['jenis_perusahaan'] = null;
            $data['emergency_status'] = null;
            $data['jabatan'] = null;
            $data['status_pegawai'] = null;
            $data['gender'] = null;
            $data['status_nikah'] = null;
            $data['company'] = null;
            $data['nama_perusahaan'] = null;
            return view('profile', $data);
        }
    }

    public function addOrEdit(Request $request)
    {
        $user = (new Customer)->where('user_id', Auth::user()->id)->first();
        $data = $this->setData($request);

        if(@count($user) == 0) {
            (new Customer)->create($data);
        }
        else {
            (new Customer)->where('user_id', Auth::user()->id)->update($data);
        }

        (new User)->where('email', $request->email)->update(['name' => $request->name]);
        session()->put('success', 'Berhasil mengubah data');
        return redirect()->back();
    }

    private function setData($request)
    {
        $data = [
            'user_id'               => Auth::user()->id,
            'nik'                   => number_back($request->nik),
            'npwp'                  => number_back($request->npwp),
            'hp'                    => $request->hp,
            'alamat'                => $request->alamat,
            'status_tinggal'        => $request->status_tinggal,
            'jenis_perusahaan'      => $request->jenis_perusahaan,
            'nama_perusahaan'       => $request->nama_perusahaan,
            'alamat_perusahaan'     => $request->alamat_perusahaan,
            'jabatan'               => $request->jabatan,
            'status_pegawai'        => $request->status_pegawai,
            'gender'                => $request->gender,
            'status_nikah'          => $request->status_nikah,
            'gaji'                  => number_back($request->gaji),
            'pinjaman_lain'         => $request->pinjaman_lain,
            'penghasilan_lain'      => number_back($request->penghasilan_lain),
            'emergency_status'      => $request->emergency_status,
            'emergency_nama'        => $request->emergency_nama,
            'emergency_hp'          => $request->emergency_hp,
        ];

        return $data;
    }
}

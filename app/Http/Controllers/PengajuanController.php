<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Customer;
use App\Eform;
use App\Pegawai;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Auth;

class PengajuanController extends Controller
{
    public function index()
    {
        $customer = (new Customer)->where('user_id', Auth::user()->id)->first();

        if($customer) {
            return view('pengajuan.index', $customer);
        }
        $data['user_id'] = null;
        return view('pengajuan.index', $data);
    }

    public function indexKsm(Request $request)
    {
        $params     = $request->all();
        $user       = (new Customer)->where('user_id', Auth::user()->id)->first();
        $eform      = (new Eform)->where('customer_id', Auth::user()->id)->where('product_type', 'ksm')->where('status', '!=', 'Approved')->get();

        if($params) {
            $return = $this->KSMEfektif($params, $user);

            $data['rincian'] = $return['rincian'];
            $data['tabel']   = $return['tabel_angsuran'];
            $data['message'] = $return['message'];
            return view('pengajuan.ksm', $data);
        }
        else {
            if(@count($eform) == 1) {

                session()->put('error', 'Kamu sedang dalam pengajuan KSM');
                return redirect()->back();
            }
            else {
                return view('pengajuan.ksm');
            }
        }
    }

    public function postKsm(Request $request)
    {
        $user    = (new Customer)->where('user_id', Auth::user()->id)->first();
        $branch  = $user->company->branch->kode_branch;

        $pegawai = (new Pegawai)->where('branch_id', $branch)->where('role', 'mks')->first();
        $appno   = rand(000001,999999);

        //upload file
        $path      = public_path('storage/'.Auth::user()->name);

            $extensionktp = $request->ktp->getClientOriginalExtension();
            $ktp  = Auth::user()->id.'-ktp.'.$extensionktp;

            $extensionnpwp = $request->npwp->getClientOriginalExtension();
            $npwp  = Auth::user()->id.'-npwp.'.$extensionnpwp;

            $extensionkk = $request->kk->getClientOriginalExtension();
            $kk  = Auth::user()->id.'-kk.'.$extensionkk;

        if(!File::exists($path)) {
            Storage::makeDirectory(Auth::user()->name);
        }

        if($request->hasFile('ktp')) {
            $image        = $request->ktp;
            Image::make($image->getRealPath())->resize(300,300)->save($path.'/'.$ktp);
        }
        if($request->hasFile('npwp')) {
            $image        = $request->npwp;
            Image::make($image->getRealPath())->resize(300,300)->save($path.'/'.$npwp);
        }
        if($request->hasFile('kk')) {
            $image        = $request->kk;
            Image::make($image->getRealPath())->resize(300,300)->save($path.'/'.$kk);
        }
        //end upload file


        $data = [
            'customer_id'       => Auth::user()->id,
            'product_type'      => 'ksm',
            'appno'             => 'Mandiri-'.$appno,
            'limit'             => number_back($request->limit),
            'angsuran'          => number_back($request->angsuran),
            'tenor'             => $request->tenor,
            'branch_id'         => $branch,
            'pn'                => $pegawai->pn,
            'status'            => 'Pending',
            'ktp'               => $ktp,
            'npwp'              => $npwp,
            'kk'                => $kk,
        ];

        (new Eform)->create($data);

        session()->put('success', 'Berhasil melakukan pengajuan baru');
        return redirect('monitoring');
    }

    private function KSMEfektif($params, $user)
    {
        $user       = (new Customer)->where('user_id', Auth::user()->id)->first();
        $price       = number_back($params['price']);
        $term        = $params['term'];
        $totalgaji = $user->gaji + $user->pendapatan_lain - $user->pinjaman_lain;
        $dbr         = round((60/100) * $totalgaji);
        $admin      = '100000';
        $provisi    = (1/100) * $price;

        if($user['jenis_perusahaan'] == 1) {
            if($term >= 12 AND $term <=24) {
                $rate = 13.5;
            }
            elseif($term > 24 AND $term <= 60) {
                $rate = 14;
            }
            elseif($term > 60) {
                $rate = 15;
            }
        }
        elseif($user['jenis_perusahaan'] == 2) {
            $rate        = 10;
        }
        else {
            $rate        = 20;
        }


        $plafond   = $price;
        $plafonds  = $plafond;
        $n         = $term + 1;
        $returnVal = [];

        $angsuranTot = round(((($rate / 12) / 100) * $plafond) / (1 - (1 / pow((1.00 + (($rate / 100) / 12.00)), $term))));

        for($i = 0; $i < $n; $i++){
            if($i == 0){
                $returnVal[$i] = [
                    "bulan"          => $i,
                    "sisa_pinjaman"  => format_rupiah(round($plafond)),
                    "angsuran_pokok" => 0,
                    "angsuran_bunga" => 0,
                    "angsuran"       => 0,
                    "bunga"          => "0%",
                ];
            }else{
                $angsuranBunga = ((($rate / 12) / 100) * $plafond);
                $angsuranPokok = $angsuranTot - $angsuranBunga;
                $plafond -= $angsuranPokok;
                if($plafond < 100)
                {
                    $plafond = '0';
                }
                $returnVal[$i] = [
                    "bulan"          => $i,
                    "sisa_pinjaman"  => format_rupiah(round($plafond)),
                    "angsuran_pokok" => format_rupiah(round($angsuranPokok)),
                    "angsuran_bunga" => format_rupiah(round($angsuranBunga)),
                    "angsuran"       => format_rupiah(round($angsuranPokok) + round($angsuranBunga)),
                    "bunga"          => $rate."%",
                ];

                $angsuran = round($angsuranPokok) + round($angsuranBunga);
            }
        }

        $rincian = [
            "plafond"           => format_rupiah($plafonds),
            "suku_bunga"        => $rate,
            "lama_pinjaman"     => $term,
            "angsuran_perbulan" => format_rupiah($angsuran),
            "user"              => $user,
            "admin"             => format_rupiah($admin),
            "provisi"           => format_rupiah($provisi),
        ];

        if($angsuran > $dbr) {
            if($term > 180) {
                $message = [
                    'alert' => 'danger',
                    'message' => 'Mohon maaf, limit terlalu besar. Silahkan mengurangi jumlah limit pinjaman'
                ];
            }
            else {
                $message = [
                    'alert' => 'danger',
                    'message' => 'Mohon maaf, limit terlalu besar. Silahkan mengurangi jumlah limit pinjaman atau menambah tenor pinjamannya'
                ];
            }
        }
        else {
            $message = [
                'alert'   => 'success',
                'message' => 'Perkiraan angsuran bulanan kamu sebagai berikut:'
            ];
        }

        $hasil = [
            "rincian"           => $rincian,
            "tabel_angsuran"    => $returnVal,
            "message"           => $message,
        ];

        return $hasil;
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Eform;
use App\Pegawai;


class EformController extends Controller
{
    public function getData()
    {
    	$pegawai = (new Pegawai)->where('pn', Auth::user()->username)->first();
    	$eform = (new Eform)->where('branch_id', $pegawai->branch_id)->get();
    	return response()->json($eform);
    }
}

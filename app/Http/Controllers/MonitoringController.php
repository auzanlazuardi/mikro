<?php

namespace App\Http\Controllers;

use App\Eform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MonitoringController extends Controller
{
    public function index()
    {
        $data['eform'] = (new Eform)->where('customer_id', Auth::user()->id)->get();

        return view('monitoring.index', $data);
    }

}

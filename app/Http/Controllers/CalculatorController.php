<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index(Request $request)
    {
        $params = $request->all();
        if($params) {
            if($params['type'] == 1) {
                $return = $this->generateFlat($params);
            }
            elseif($params['type'] == 2){
                $return = $this->generateEfektif($params);
            }
            $data['rincian'] = $return['rincian'];
            $data['tabel']   = $return['tabel_angsuran'];

            return view('kalkulator', $data);
        }else {
            return view('kalkulator');
        }
    }


    /**
     * Perhitungan Bunga Flat
     * @param $params
     *
     * @return array
     */

    private function generateFlat($params)
    {
        $price       = number_back($params['price']);
        $term        = $params['term'];
        $term_tahun  = $params['term'] / 12;
        $rate        = $params['rate'];

        $gaji      = 10000000;
        $plafond   = $price;
        $plafonds  = $plafond;
        $n         = $term + 1;
        $returnVal = [];

        $angsuranPokok = $plafond / $term;
        $angsuranBunga = (($rate / 12) /100) * $plafond;
        $angsuran      = (round($angsuranPokok) + round($angsuranBunga));

        for($i = 0; $i < $n; $i++)
        {
            if($i == 0){
                $returnVal[$i] = [
                    "bulan"          => $i,
                    "sisa_pinjaman"  => format_rupiah((int)$plafond),
                    "angsuran_pokok" => 0,
                    "angsuran_bunga" => 0,
                    "angsuran"       => 0,
                    "bunga"          => "0%",
                ];
            }else{
                $plafond -= $angsuranPokok;
                if($plafond < 100)
                {
                    $plafond = '0';
                }
                $returnVal[$i] = [
                    "bulan"          => $i,
                    "sisa_pinjaman"  => format_rupiah(round((int)$plafond)),
                    "angsuran_pokok" => format_rupiah(round($angsuranPokok)),
                    "angsuran_bunga" => format_rupiah(round($angsuranBunga)),
                    "angsuran"       => format_rupiah($angsuran),
                    "bunga"          => $rate."%",
                ];
            }
        }

        $rincian = [
            "plafond"           => format_rupiah($plafonds),
            "suku_bunga"        => $rate."%",
            "lama_pinjaman"     => $term." Bulan",
            "lama_tahunan"      => $term_tahun." Tahun",
            "angsuran_perbulan" => format_rupiah($angsuran),
        ];

        $hasil = [
            "rincian"           => $rincian,
            "tabel_angsuran"    => $returnVal,
        ];

        return $hasil;
    }

    private function generateEfektif($params)
    {
        $price       = number_back($params['price']);
        $term        = $params['term'];
        $rate        = $params['rate'];

        $plafond   = $price;
        $plafonds  = $plafond;
        $n         = $term + 1;
        $returnVal = [];

        $angsuranTot = round(((($rate / 12) / 100) * $plafond) / (1 - (1 / pow((1.00 + (($rate / 100) / 12.00)), $term))));

        for($i = 0; $i < $n; $i++){
            if($i == 0){
                $returnVal[$i] = [
                    "bulan"          => $i,
                    "sisa_pinjaman"  => format_rupiah(round($plafond)),
                    "angsuran_pokok" => 0,
                    "angsuran_bunga" => 0,
                    "angsuran"       => 0,
                    "bunga"          => "0%",
                ];
            }else{
                $angsuranBunga = ((($rate / 12) / 100) * $plafond);
                $angsuranPokok = $angsuranTot - $angsuranBunga;
                $plafond -= $angsuranPokok;
                if($plafond < 100)
                {
                    $plafond = '0';
                }
                $returnVal[$i] = [
                    "bulan"          => $i,
                    "sisa_pinjaman"  => format_rupiah(round($plafond)),
                    "angsuran_pokok" => format_rupiah(round($angsuranPokok)),
                    "angsuran_bunga" => format_rupiah(round($angsuranBunga)),
                    "angsuran"       => format_rupiah(round($angsuranPokok) + round($angsuranBunga)),
                    "bunga"          => $rate."%",
                ];

                $angsuran = round($angsuranPokok) + round($angsuranBunga);
            }
        }

        $rincian = [
            "plafond"           => format_rupiah($plafonds),
            "suku_bunga"        => $rate."%",
            "lama_pinjaman"     => $term." Bulan",
            "angsuran_perbulan" => format_rupiah($angsuran),
        ];

        $hasil = [
            "rincian"           => $rincian,
            "tabel_angsuran"    => $returnVal,
        ];

        return $hasil;
    }


}

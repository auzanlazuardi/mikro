<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (strtolower($this->method()))
        {
            case 'post':
                if($this->segment(1) == 'profile') {
                    return [
                        'nik'       => 'required|max:16',
                        'hp'        => 'required|max:20'
                    ];
                }
                break;
            default:
                return [];
                break;
        }
    }
}

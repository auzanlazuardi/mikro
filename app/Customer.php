<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = [
        'user_id', 'nik', 'npwp', 'hp', 'alamat', 'status_tinggal',
        'jenis_perusahaan', 'nama_perusahaan', 'alamat_perusahaan', 'kodepos_perusahaan', 'gaji', 'penghasilan_lain',
        'emergency_status', 'emergency_nama', 'emergency_hp', 'emergency_alamat', 'emergency_kota', 'jabatan', 'pinjaman_lain', 'status_pegawai', 'status_nikah', 'gender'
    ];

    protected $appends = ['statusNikahText', 'genderText', 'jenisPerusahaanText', 'jabatanText', 'statusPegawaiText', 'gajiText', 'penghasilanLainText', 'statusEmergencyText'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'nama_perusahaan', 'kode');
    }

    public function getStatusNikahTextAttribute()
    {
        if($this->status_nikah == 1) {
            return 'Belum Menikah';
        }
        elseif($this->status_nikah == 2) {
            return 'Menikah';
        }
        elseif($this->status_nikah == 3) {
            return 'Janda/Duda';
        }
    }

    public function getGenderTextAttribute()
    {
        if($this->gender == 1) {
            return 'Laki-laki';
        }
        elseif($this->gender == 2) {
            return 'Perempuan';
        }
    }

    public function getJenisPerusahaanTextAttribute()
    {
        if($this->jenis_perusahaan == 1) {
            return 'PNS/TNI/POLRI';
        }
        elseif($this->jenis_perusahaan == 2) {
            return 'BUMN/BUMD';
        }
        elseif($this->jenis_perusahaan == 3) {
            return 'SWASTA';
        }
    }

    public function getJabatanTextAttribute()
    {
        if($this->jabatan == 1) {
            return 'Pemilik/Komisaris';
        }
        elseif($this->jabatan == 2) {
            return 'Direktur';
        }
        elseif($this->jabatan == 3) {
            return 'Pengurus';
        }
    }

    public function getStatusPegawaiTextAttribute()
    {
        if($this->status_pegawai == 1) {
            return 'Tetap';
        }
        elseif($this->status_pegawai == 2) {
            return 'Kontrak';
        }
    }

     public function getGajiTextAttribute()
    {
        return 'Rp '.format_rupiah($this->gaji);
    }

    public function getPenghasilanLainTextAttribute()
    {
        return 'Rp '.format_rupiah($this->penghasilan_lain);
    }

    public function getStatusEmergencyTextAttribute()
    {
        if($this->emergency_status == 1) {
            return 'ORANG TUA';
        }
        elseif($this->emergency_status == 2) {
            return 'SUAMI/ISTRI';
        }
        elseif($this->emergency_status == 3) {
            return 'SAUDARA KANDUNG';
        }
    }
}
